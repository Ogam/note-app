import React from "react";
const Task = (props) => {
  return (
    <div>
      <h3>
        {props.index}. {props.task.title}
      </h3>
      <p>{props.task.body}</p>
      <button onClick={() => props.remove(props.task.title)}>delete</button>
    </div>
  );
};
export default Task;
