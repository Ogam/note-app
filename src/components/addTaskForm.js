import React, { useState } from "react";
const AddTaskForm = (props) => {
  var [title, setTitle] = useState("");
  var [body, setBody] = useState("");
  let addTask = (e) => {
    e.preventDefault();
    props.dispatch({
      type: "ADD_TASK",
      title,
      body,
    });
    setTitle("");
    setBody("");
  };
  return (
    <div>
      <form onSubmit={addTask}>
        <input
          placeholder="task title"
          value={title}
          onChange={(evt) => setTitle(evt.target.value)}
        />
        <textarea
          placeholder="task body"
          value={body}
          onChange={(evt) => setBody(evt.target.value)}
        />
        <button>Add Task</button>
      </form>
    </div>
  );
};

export default AddTaskForm;
