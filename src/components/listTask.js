import React from "react";
import Task from "./tasks";
const ListTask = (props) => {
  return props.tasks.map((task, index) => {
    return (
      <Task
        key={task.title}
        task={task}
        remove={props.removeTask}
        index={index + 1}
      />
    );
  });
};
export default ListTask;
