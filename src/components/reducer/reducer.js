let reducer = (state, action) => {
  switch (action.type) {
    case "SHOW_TASK":
      return action.tasks;
    case "ADD_TASK":
      return [...state, { title: action.title, body: action.body }];
    case "REMOVE":
      return state.filter((task) => task.title !== action.title);
    default:
      return state;
  }
};

export default reducer;
