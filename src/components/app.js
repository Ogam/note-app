import React, { useReducer, useEffect } from "react";
import reducer from "./reducer/reducer";
import ListTask from "./listTask";
import AddTaskForm from "./addTaskForm";
const App = () => {
  var [tasks, dispatch] = useReducer(reducer, []);
  var removeTask = (title) => {
    dispatch({
      type: "REMOVE",
      title,
    });
  };
  useEffect(() => {
    var taskInfo = JSON.parse(localStorage.getItem("tasks"));
    if (taskInfo) {
      dispatch({
        type: "SHOW_TASK",
        tasks: taskInfo,
      });
    }
  }, []);
  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);
  return (
    <div>
      <h1>Task App</h1>
      <ListTask tasks={tasks} removeTask={removeTask} />
      <AddTaskForm dispatch={dispatch} />
    </div>
  );
};

export default App;
